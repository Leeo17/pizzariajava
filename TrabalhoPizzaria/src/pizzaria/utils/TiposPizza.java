package pizzaria.utils;

import pizzaria.objetos.TipoPizza;

public class TiposPizza {
    
    public static TipoPizza pizzaSimples = new TipoPizza("Simples");
    public static TipoPizza pizzaEspecial = new TipoPizza("Especial");
    public static TipoPizza pizzaPremium = new TipoPizza("Premium");
    
}
